﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication2.Models;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/
        public ActionResult Login()
        {
            return View();
        }
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Login(string email,string password)
        {
            s1041630Entities db = new s1041630Entities();
            var user = db.User.Where(d => d.Email == email && d.Password == password).FirstOrDefault();
            if(user!=null){
                return RedirectToAction("Index");
            }
            else{
                ViewBag.Message = "帳號或密碼錯誤，請重新輸入";
                return View();
            }
        }
	}
}